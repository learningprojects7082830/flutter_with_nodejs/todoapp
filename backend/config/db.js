const mongoose = require('mongoose');


const connection = mongoose.createConnection('mongodb://localhost:27017/FlutterTodo').on('open', ()=>{
    console.log(`MongoDB Connection Successfull`);
}).on('error', ()=>{
    console.log(`MongoDB Connection Failed`)
});

module.exports = connection;