const UserService = require('../services/user.services');

exports.register = async (req, res, next)=>{
    try {
        const {email,password} = req.body;
        const successResp = await UserService.registerUser(email, password);
        res.json({status: true, success: "User Registration Successfull"});
    } catch (error) {
        throw error;
    }
};